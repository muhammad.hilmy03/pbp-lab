from django.core.files.base import ContentFile
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.db import models
from django.contrib.auth.decorators import login_required
from lab_1.models import Friend
from .forms import FriendForm

# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
    response = {'friends': Friend.objects.all()}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def add_friend(request):
    context = {}

    form = FriendForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/lab-3')
        
    context['form'] = form
    return render(request, 'lab3_form.html', context)
