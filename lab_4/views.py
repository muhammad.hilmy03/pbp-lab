from django.http import response
from django.shortcuts import get_object_or_404, render
from django.http.response import Http404, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from lab_2.models import Note
from .forms import NoteForm
from django.views import generic

# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
    response = {'notes': Note.objects.all()}
    return render(request, 'lab4_index.html', response)

@login_required(login_url="/admin/login/")
def add_note(request):
    context = {}

    form = NoteForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/lab-4')
        
    context['form'] = form
    return render(request, 'lab4_form.html', context)

@login_required(login_url="/admin/login/")
def note_list(request):
    response = {'notes': Note.objects.all()}
    return render(request, 'lab4_note_list.html', response)

def notes_detail(request,id):
    Notes = get_object_or_404(Note, pk=id)
    context = {'notes': Notes}
    return render(request, 'lab4_note_details.html',context)

