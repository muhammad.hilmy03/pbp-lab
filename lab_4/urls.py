from django.urls import path
from .views import add_note, index, note_list, notes_detail

urlpatterns = [
    path('', index, name='index'),
    path('add-note', add_note),
    path('note-list', note_list),

    path('note/<int:id>', notes_detail, name='notes_detail'),
]