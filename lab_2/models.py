from django.db import models

# Create your models here.
class Note(models.Model):
    note_to = models.CharField(max_length = 100)
    note_from = models.CharField(max_length = 100)
    note_title = models.CharField(max_length = 100)
    note_msg = models.TextField()