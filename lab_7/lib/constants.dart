import 'package:flutter/material.dart';

const colorBlue = Color(0xFF003566);
const colorDarkBlue = Color(0xFF001D3D);
const colorLightBlue = Color(0xFFe8f0fe);
const colorYellow1 = Color(0xFFFFC300);
const colorYellow2 = Color(0xFFFFD60A);
