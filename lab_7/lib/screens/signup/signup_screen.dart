// @dart=2.9
// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:lab_7/constants.dart';
import 'package:lab_7/screens/login/login_screen.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class SignUpForm extends StatefulWidget {
  const SignUpForm({Key key}) : super(key: key);

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

Future<bool> uname_check(String uname) async {
    String url = "https://slowlab-core.herokuapp.com/auth/ajax/uname_verif?username=$uname";
    print(url);
    try{
      final response = await http.get(Uri.parse(url));
      // print(response.body);
      Map<String, bool> extractedData = jsonDecode(response.body);
      return extractedData["user"];
    }catch (error){
      print("ERORRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR");
      return false;
    }
  }

class _SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                color: colorBlue,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            margin: EdgeInsets.all(50),
            child: Column(
              children: [
                Text(
                  "SignUp",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 32),
                ),
                SizedBox(height: size.height * 0.03),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  decoration: BoxDecoration(
                    color: colorLightBlue,
                    borderRadius: BorderRadius.circular(29),
                  ),
                  child: TextFormField(
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                      hintText: "Username",
                      icon: Icon(
                        Icons.person,
                        color: colorBlue,
                      ),
                      border: InputBorder.none,
                    ),
                    validator: (value) {
                      // print(uname_check(value));
                      if (value.isEmpty) {
                        return 'Username Tidak boleh kosong';
                      }
                      // if (uname_check(value) != true){
                      //   return 'username sudah terdaftar';
                      // }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  decoration: BoxDecoration(
                    color: colorLightBlue,
                    borderRadius: BorderRadius.circular(29),
                  ),
                  child: TextFormField(
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                      hintText: "Email",
                      icon: Icon(
                        Icons.mail,
                        color: colorBlue,
                      ),
                      border: InputBorder.none,
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Email Tidak boleh kosong';
                      } if (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)){
                        return 'Email Tidak Valid';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  decoration: BoxDecoration(
                    color: colorLightBlue,
                    borderRadius: BorderRadius.circular(29),
                  ),
                  child: TextFormField(
                    obscureText: true,
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                      hintText: "Password",
                      icon: Icon(
                        Icons.lock,
                        color: colorBlue,
                      ),
                      border: InputBorder.none,
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Password Tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                ElevatedButton(
                  child: Text(
                    "SignUp",
                    style: TextStyle(color: Colors.black),
                  ),
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: colorYellow2))),
                    backgroundColor:
                        MaterialStateProperty.all<Color>(colorYellow2),
                  ),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return LoginForm();
                          },
                        ),
                      );
                    }
                  },
                ),
                SizedBox(height: size.height * 0.03),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Already have an account?  ",
                      style: TextStyle(color: Colors.white),
                    ),
                    ElevatedButton(
                      child: Text(
                        "Login",
                        style: TextStyle(color: Colors.black),
                      ),
                      style: ButtonStyle(
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                    side: BorderSide(color: colorYellow2))),
                        backgroundColor:
                            MaterialStateProperty.all<Color>(colorYellow2),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) {
                              return LoginForm();
                            },
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
