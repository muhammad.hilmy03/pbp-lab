// @dart=2.9
// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:lab_7/constants.dart';
import 'package:lab_7/screens/signup/signup_screen.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class LoginForm extends StatefulWidget {
  const LoginForm({Key key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

Future<bool> uname_check(uname) async {
    const url = 'https://slowlab-core.herokuapp.com/';
    try {
      final response = await http.get(Uri.parse(url));
      print(response.body);
      Map<String, dynamic> extractedData = jsonDecode(response.body);
      if (extractedData["user"] == "false"){
        return false;
      }else{
        return true;
      }
    } catch (error) {
      print(error);
      return null;
    }
  }


class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                color: colorBlue,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            margin: EdgeInsets.all(50),
            child: Column(
              children: [
                Text(
                  "Login",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 32),
                ),
                
                SizedBox(height: size.height * 0.03),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  decoration: BoxDecoration(
                    color: colorLightBlue,
                    borderRadius: BorderRadius.circular(29),
                  ),
                  child: TextFormField(
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                      hintText: "Username",
                      icon: Icon(
                        Icons.person,
                        color: colorBlue,
                      ),
                      border: InputBorder.none,
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Username Tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  decoration: BoxDecoration(
                    color: colorLightBlue,
                    borderRadius: BorderRadius.circular(29),
                  ),
                  child: TextFormField(
                    obscureText: true,
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                      hintText: "Password",
                      icon: Icon(
                        Icons.lock,
                        color: colorBlue,
                      ),
                      border: InputBorder.none,
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Password Tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                ElevatedButton(
                  child: Text(
                    "Login",
                    style: TextStyle(color: Colors.black),
                  ),
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: colorYellow2))),
                    backgroundColor:
                        MaterialStateProperty.all<Color>(colorYellow2),
                  ),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return LoginForm();
                          },
                        ),
                      );
                    }
                  },
                ),
                SizedBox(height: size.height * 0.03),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Don't have an account?  ",
                      style: TextStyle(color: Colors.white),
                    ),
                    ElevatedButton(
                      child: Text(
                        "Sign-Up",
                        style: TextStyle(color: Colors.black),
                      ),
                      style: ButtonStyle(
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                    side: BorderSide(color: colorYellow2))),
                        backgroundColor:
                            MaterialStateProperty.all<Color>(colorYellow2),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) {
                              return SignUpForm();
                            },
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
