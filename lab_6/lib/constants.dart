import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF003566);
const kPrimaryLightColor = Color(0xFFb4dbff);
const kSecondaryColor = Color(0xFFFFD60A);
