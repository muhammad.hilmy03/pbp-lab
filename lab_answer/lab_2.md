# Jawaban Lab-2

## 1. Apakah perbedaan antara JSON dan XML?
JSON merupakan format file yang digunakan untuk menyimpan dan mentransfer data objek yang berisikan attribute-value dan arrays. XML adalah eXtensible markup language yang juga digunakan untuk menyimpan dan mentransfer data

| NO | JSON                          | XML                               |
|:--:| ----------------------------- | --------------------------------- |
| 1  | JSON berbentuk notasi object  | XML berbentuk *markup language*   |
| 2  | JSON hanya men-*support* data numerik dan teks | XML men-*support* berbagai macam data seperti angka, teks, gambar, grafik tabel, dan lain-lain. |
| 3  | JSON tidak memiliki kemampuan untuk menampilkan display | XML memiliki kemampuan untuk menampilkan data karena XML merupakan markup language |
| 4  | JSON Kurang aman              | XML Lebih aman di bandingkan JSON |
| 5  | JSON hanya men-*support encoding* UTF-8   | XML mensupport berbagai macam format *encoding*|

### Contoh JSON:
```javascript
[
   {
      "model":"lab_2.note",
      "pk":1,
      "fields":{
         "note_to":"dia",
         "note_from":"aku",
         "note_title":"hallo",
         "note_msg":"apa kabar kamu yang jauh disana?"
      }
   },
   {
      "model":"lab_2.note",
      "pk":2,
      "fields":{
         "note_to":"me",
         "note_from":"saya",
         "note_title":"gatau nulis apa",
         "note_msg":"hallooo sayaa gatau mau nulis apa tapi disuruh ada messagenya jadi random aja gitu nulis ginian yang sebenernya engga ada intinya juga sih nulis ginian biar keliatan penuh aja gitu"
      }
   }
]
```

### Contoh XML:
```XML
<django-objects version="1.0">
    <object model="lab_2.note" pk="1">
        <field name="note_to" type="CharField">dia</field>
        <field name="note_from" type="CharField">aku</field>
        <field name="note_title" type="CharField">hallo</field>
        <field name="note_msg" type="TextField">apa kabar kamu yang jauh disana?</field>
    </object>
    <object model="lab_2.note" pk="2">
        <field name="note_to" type="CharField">me</field>
        <field name="note_from" type="CharField">saya</field>
        <field name="note_title" type="CharField">gatau nulis apa</field>
        <field name="note_msg" type="TextField">hallooo sayaa gatau mau nulis apa tapi disuruh ada messagenya jadi random aja gitu nulis ginian yang sebenernya engga ada intinya juga sih nulis ginian biar keliatan penuh aja gitu</field>
    </object>
</django-objects>
```

---

## 2. Apakah perbedaan antara HTML dan XML?
HTML adalah markup language yang digunakan untuk membuat layout dan struktur dari sebuah web, sedangkan XML merupakan suatu markup language yang didesign untuk menyimpan dan mentransfer data.

| NO | HTML                          | XML                               |
|:--:| ----------------------------- | --------------------------------- |
| 1  | HTML digunakan untuk membuat layout dan struktur | XML digunakan untuk mentransfer data |
| 2  | HTML tidak case sensitive | XML case sensitive |
| 3  | HTML tidak selalu memerlukan closing tag | XML harus menggunakan closing tag|
| 4  | HTML *Format driven* | XML *Content driven* |
| 5  | HTML tidak dapat menggunakan namespaces | XML dapat menggunakan namespaces|

### Contoh HTML:
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>{{ name }}</title>
  </head>
  <body>
    <h1>Hello my name is {{ name }}</h1>
    <article>NPM : {{ npm }}</article>
    <article>I am {{ age }} years old</article>
  </body>
</html>

```

### Contoh XML:
```xml
<django-objects version="1.0">
    <object model="lab_2.note" pk="1">
        <field name="note_to" type="CharField">dia</field>
        <field name="note_from" type="CharField">aku</field>
        <field name="note_title" type="CharField">hallo</field>
        <field name="note_msg" type="TextField">apa kabar kamu yang jauh disana?</field>
    </object>
    <object model="lab_2.note" pk="2">
        <field name="note_to" type="CharField">me</field>
        <field name="note_from" type="CharField">saya</field>
        <field name="note_title" type="CharField">gatau nulis apa</field>
        <field name="note_msg" type="TextField">hallooo sayaa gatau mau nulis apa tapi disuruh ada messagenya jadi random aja gitu nulis ginian yang sebenernya engga ada intinya juga sih nulis ginian biar keliatan penuh aja gitu</field>
    </object>
</django-objects>
```

## Referensi
Martin, M. (2021, August 27). Difference between XML and HTML. Guru99. Retrieved September 27, 2021, from https://www.guru99.com/xml-vs-html-difference.html.\
Walker, A. (2021, August 27). JSON vs XML: What's the difference? Guru99. Retrieved September 27, 2021, from https://www.guru99.com/json-vs-xml-difference.html. 